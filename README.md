# Getting Started #

```
git clone https://bitbucket.org/genomicepidemiology/lre-finder.git
cd lre-finder && make

tar -xvzf elmDB.tar.gz
kma index -i elmDB/elm.fsa -o elmDB/elm
python LRE-Finder.py -i reads_se.fq.gz -o output/name -t_db elmDB/elm -ID 80 -1t1 -cge -matrix
python LRE-Finder.py -ipe reads_1.fq.gz reads_2.fq.gz -o output/name -t_db elmDB/elm -ID 80 -1t1 -cge -matrix
```

# Introduction #
LRE-Finder is designed to identify genes and mutations leading to linezolid resistance in
*Enterococci faecalis* and *E. faecium*. It does so by postprocessing the alignments performed by KMA,
where the SNPs of interest is located in polyclonal genes (23S).
Because of this polyclonal feature it is necesarry to analyze the raw reads directly, as assemblies usually
merge these regions into one representative.
If you use LRE-Finder for your published research, then please cite: 

1. Henrik HASMAN, Philip T.L.C. CLAUSEN, Hulya KAYA, Frank HANSEN, Jenny Dahl KNUDSEN, Mikala WANG, Barbara Juliane HOLZKNECHT, Jurgita SAMULIONIENE, Bent ROEDER, Niels FRIMODT-MOELLER, Ole LUND, and Anette M. HAMMERUM,
"LRE-Finder, a Web Tool for detection of the 23S rRNA mutations, and the *optrA, cfr, cfr*(B) and *poxtA* genes, encoding linezolid resistance in Enterococci from whole genome sequences" 

2. Philip T.L.C. Clausen, Frank M. Aarestrup & Ole Lund, 
"Rapid and precise alignment of raw reads against redundant databases with KMA", 
BMC Bioinformatics, 2018;19:307.


# Usage #
For practical reasons you might want to add the two programs (LRE-Finder.py and getGene)
to your path, this is usually done with:

```
cp LRE-Finder.py ~/bin/
mv getGene ~/bin/
```

LRE-Finder takes the exact same arguments as KMA, but requires additional index files (\*.genepos and \*.mutdist).
The \*.genepos is needed in order to use getGene, which extracts the target positions from the KMA alignments.
The first field must be the target sequence name, which is followed by the target positions. 
From the elm database it can be seen that the target postions are 2505 2576 in the sequences: 
"23S_Enterococcus_faecium" and "23S_Enterococcus_faecalis".
The \*.mutdist is needed by LRE-Finder.py to determine whether the amounts detected SNPs are high enough to
confer resistance. One entry is needed for each target sequence (specified by '>'), where an optional tab-separated 
field can be added definning an alternative name in output. This should be immediately followed by the target SNPs, 
together with the proportion of the target SNP that is needed to confer resistance. 
The notation of target SNPs is as follows: (Wild type base)(position)(mutant base(s)). For example:
### A2142CG ###
Defines a SNP at position 2142, where the wild type is 'A' while 'C' and 'G' will confer resistance.

# Installation Requirements #
In order to install and run LRE-Finder, you need to have a C-compiler, python interpreter and KMA installed.
KMA can be found at: *https://bitbucket.org/genomicepidemiology/kma/*

# Help #
Usage and options are available with the "-h" option both programs.
If in doubt, please mail any concerns or problems to: *plan@dtu.dk*.

# Citation #
1. Henrik HASMAN, Philip T.L.C. CLAUSEN, Hulya KAYA, Frank HANSEN, Jenny Dahl KNUDSEN, Mikala WANG, Barbara Juliane HOLZKNECHT, Jurgita SAMULIONIENE, Bent ROEDER, Niels FRIMODT-MOELLER, Ole LUND, and Anette M. HAMMERUM, "LRE-Finder, a Web Tool for detection of the 23S rRNA mutations, and the *optrA, cfr, cfr*(B) and *poxtA* genes, encoding linezolid resistance in Enterococci from whole genome sequences"
2. Philip T.L.C. Clausen, Frank M. Aarestrup & Ole Lund, "Rapid and precise alignment of raw reads against redundant databases with KMA", BMC Bioinformatics, 2018;19:307.

# License #
Copyright (c) 2018, Philip Clausen, Technical University of Denmark
All rights reserved.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
